<?php get_header(); ?>
			<div class="content main">
				<div class="col">

					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class('cf'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
						<h1 class="page-title><?php the_title(); ?></h1>
						<?php if(get_field('subtitle')) { ?>
						<h2 class="subtitle"><?php the_field('subtitle'); ?></h2>
						<?php } ?>
						<?php if(get_field('publisher')) { ?>
						<span class="publisher"><?php the_field('publisher'); ?>, <?php the_field('published_date'); ?></span>
						<?php } ?>
						<?php if(get_field('author')) { ?>
						<span class="author"><strong>Author(s): </strong>
							<?php $author = get_field('author'); ?>
							<? if( $author ): ?>
							<?php foreach( $author as $post): ?>
							<?php setup_postdata($post); ?>
							<a href="<?php the_permalink(); ?>" class="author-name"><?php the_title(); ?></a><?php endforeach; ?><?php wp_reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_authors')) { ?>, <?php the_field('additional_authors'); ?>
							<?php } ?>
						</span>
						<?php } ?>
						<?php if(get_field('editor')) { ?>
						<span class="author"><strong>Editor(s): </strong>
							<?php $editor = get_field('author'); ?>
							<? if( $editor ): ?>
							<?php foreach( $editor as $post): ?>
							<?php setup_postdata($post); ?>
							<a href="<?php the_permalink(); ?>" class="author-name"><?php the_title(); ?></a><?php endforeach; ?><?php wp_reset_postdata(); ?><?php endif; ?><?php if(get_field('additional_editors')) { ?>, <?php the_field('additional_editors'); ?>
							<?php } ?>
						</span>
						<?php } ?>
						<section class="entry-content cf" itemprop="articleBody">
							<?php the_content(); ?>
						</section>
					</article>
					<?php endwhile; ?>
					<?php else : endif; ?>
					</div>
					<div class="col">
						<?php if(get_field('book_cover')) {
							$image = get_field('book_cover');
							if( !empty($image) ): 
								// vars
								$url = $image['url'];
								$title = $image['title'];
								// thumbnail
								$size = 'large-book';
								$thumb = $image['sizes'][ $size ];
								$width = $image['sizes'][ $size . '-width' ];
								$height = $image['sizes'][ $size . '-height' ];
							endif; ?>
							<img src="<?php echo $thumb; ?>" alt="<?php the_title(); ?> book cover" width="<?php echo $width; ?>" height="<?php echo $height; ?>" class="cover" />
							<?php } else { ?>
							<div class="custom-cover cover">
								<span class="title"><?php the_title(); ?></span>
							</div>
						<?php } ?>	
					</div>
				</div>

<?php get_footer(); ?>
