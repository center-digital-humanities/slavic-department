				<?php // For Event month or list landing page, https://gist.github.com/jo-snips/2415009
				// Only run if The Events Calendar is installed 
				if ( tribe_is_past() || tribe_is_upcoming() && !is_tax() || tribe_is_month() && !is_tax() || tribe_is_upcoming() && is_tax()) { 
					// Do nothing	
				}
				// For posts
				elseif (is_single() || is_category() || is_search()) { ?>
				<div class="col side feed" role="complementary">
					<?php if ( is_active_sidebar( 'news-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'news-sidebar' ); ?>
					<?php else : endif; ?>
					<?php if ( is_active_sidebar( 'events-sidebar' ) ) :  ?>
					<?php dynamic_sidebar( 'events-sidebar' ); ?>
					<?php else : endif; ?>
				</div>
				<?php } ?>
				<?php // For pages
				if (is_page() || is_404()) { ?>
				<div class="col side">
					<div class="content">
						<nav class="page-nav" role="navigation" aria-labelledby="section navigation">
							<?php
								// If a Graduate subpage
								if (is_tree(863) || get_field('menu_select') == "graduate") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Graduate', 'bonestheme' ),
										'menu_class' => 'graduate-nav',
										'theme_location' => 'graduate-nav',
										'before' => '',
										'after' => '',
										'depth' => 2,
										'items_wrap' => '<h3>Graduate</h3> <ul>%3$s</ul>'
									));
								}
								
								// If an Undergraduate subpage
								if (is_tree(860) || get_field('menu_select') == "undergraduate") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Undergraduate', 'bonestheme' ),
									   	'menu_class' => 'undergrad-nav',
									   	'theme_location' => 'undergrad-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Undergraduate</h3> <ul>%3$s</ul>'
									));
								}
								
								// If a Languages subpage
								if (is_tree(1282) || is_tree(1316) || is_tree(1318) || is_tree(1314) || is_tree(1312) || is_tree(1310) || is_tree(1292) || is_tree(1308) ||get_field('menu_select') == "languages") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Langauges', 'bonestheme' ),
									   	'menu_class' => 'languages-nav',
									   	'theme_location' => 'languages-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Languages</h3> <ul>%3$s</ul>'
									));
								}
								
								// If an Memoriam subpage
								if (is_tree(1846) || get_field('menu_select') == "memoriam") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Memoriam', 'bonestheme' ),
									   	'menu_class' => 'memoriam-nav',
									   	'theme_location' => 'memoriam-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Memoriam</h3> <ul>%3$s</ul>'
									));
								}
								
								// If an Academics subpage
								if (is_tree(939) || get_field('menu_select') == "academics") {
									wp_nav_menu(array(
									   	'container' => false,
									   	'menu' => __( 'Academics', 'bonestheme' ),
									   	'menu_class' => 'academics-nav',
									   	'theme_location' => 'academics-nav',
									   	'before' => '',
									   	'after' => '',
									   	'depth' => 2,
									   	'items_wrap' => '<h3>Academics</h3> <ul>%3$s</ul>'
									));
								}
								
								// For Search, 404's, or other pages you want to use it on
								// Replace 9999 with id of parent page
								if (is_tree(9999) || is_search() || is_404() || is_page('contact') || is_page('about') || get_field('menu_select') == "general") {
									wp_nav_menu(array(
										'container' => false,
										'menu' => __( 'Main Menu', 'bonestheme' ),
										'menu_class' => 'side-nav',
										'theme_location' => 'main-nav',
										'before' => '',
										'after' => '',
										'depth' => 1,
										'items_wrap' => '<h3>Main Menu</h3> <ul>%3$s</ul>'
									));
								}
							?>
						</nav>
						<?php if ( is_page( 'give' )  ) : ?>
							<?php dynamic_sidebar( 'give-sidebar' ); ?>
						<?php else : ?>
					
						<?php endif; ?>
					</div>
				</div>
					
				<?php } ?>